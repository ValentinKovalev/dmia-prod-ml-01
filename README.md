## Password Frequency
### Description
Service to predict password frequency based on training data.  
Related course: https://github.com/data-mining-in-action/DMIA_ProductionML_2021_Spring  
Related competition: https://www.kaggle.com/c/dmia-production-ml-2021-1-passwords
